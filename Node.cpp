#include "Node.h"


Node::Node(int id, const vector<trNode> &successors, const vector<transSeqLang> &languages) : id(id),
                                                                                              successors(successors),
                                                                                              languages(languages) {}

Node::Node(int id) : id(id) {}

Node::Node(int id, string nom) {
    this->id = id;
    this->name = nom;
}

Node::~Node() = default;

int Node::getId() const {
    return id;
}

string Node::getName() const {
    return name;
}

void Node::setId(int id) {
    Node::id = id;
}

vector<trNode> &Node::getSuccessors() {
    return successors;
}

void Node::setSuccessors(const vector<trNode> &successors) {
    Node::successors = successors;
}

vector<transSeqLang> &Node::getLanguages() {
    return languages;
}

void Node::setLanguages(const vector<transSeqLang> &languages) {
    Node::languages = languages;
}

void Node::printLg(){
     cout << "Regular Expression of : "<<getName()<<endl<<"{"; 


        for (const auto& l: Lg) {
            if(l.second!=nullptr)
            {cout<< "< "<< l.first <<" , "<<l.second->getName()<<" > , " ;}
            else
            {
                cout<< "< "<< l.first <<" , null > , " ;
            }
            
        }
        cout<<" }"<<endl;
}
void Node::printCyandComp(){
     cout << "Cy :  "<<getName()<<endl<<"{"; 


        for (const auto& l: Cy) {
            if(l.second!=nullptr)
            {cout<< "< "<< l.first <<" , "<<l.second->getName()<<" > , " ;}
            else
            {
                cout<< "< "<< l.first <<" , null > , " ;
            }
            
        }
        cout<<" }"<<endl;

        cout << "CompCy :  "<<getName()<<endl<<"{"; 


        for (const auto& l: CompCy) {
            if(l.second!=nullptr)
            {cout<< "< "<< l.first <<" , "<<l.second->getName()<<" > , " ;}
            else
            {
                cout<< "< "<< l.first <<" , null > , " ;
            }
            
        }
        cout<<" }"<<endl;
}
int Node::sizeLg(){
    int nb=0;
     cout << "Nb d'elements de LG : "<<endl; 

        for (const auto& l: Lg) {
            nb++;
            
        }
    return nb; 
}

string Node::getLanguage(){
    string lg = "";
//    string lg="Language : "+ getName() +": \n";

        for (const auto& l: Lg) {
            if(l.second!=nullptr)
            {
                lg+=l.first+".Lg ( "+ l.second->getName() +" )+";
            }
            else
            {
                lg+=l.first+"+";
            }
        }
        lg.pop_back();

        return lg;
        
}

