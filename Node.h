#ifndef IMPLEM_NODE_H
#define IMPLEM_NODE_H

#include "Graph.h"
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <iostream>

using namespace std;

class Node;

typedef string transition;
typedef std::pair<transition, Node*> trNode;
typedef string transitionSequence;
typedef pair<transitionSequence, Node*> transSeqLang;

class Node {
private:
    int id;
    string name;
    vector<trNode> successors;
    vector<transSeqLang> languages;

    

public:
    // To count nbPaths
    vector<int> nbPaths;
    vector<transSeqLang> Cy;
    vector<transSeqLang> CompCy;
    vector<transSeqLang> Lg;
    vector<string> prefixes;

    Node(int id);
    Node(int id, string nom);
    Node(int id, const vector<trNode> &successors, const vector<transSeqLang> &languages);

    int getId() const;
    string getName() const;
    void setId(int id);
    vector<trNode> &getSuccessors();
    void setSuccessors(const vector<trNode> &successors);
    vector<transSeqLang> &getLanguages();
    void setLanguages(const vector<transSeqLang> &languages);

    void printLg();
    void printCyandComp();
    int sizeLg();

    string getLanguage();
    
    virtual ~Node();
};

#endif // IMPLEM_NODE_H
