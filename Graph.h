#ifndef IMPLEM_GRAPH_H
#define IMPLEM_GRAPH_H
#include "Node.h"
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <map>

using namespace std;

class Node;
typedef std::set<int> Tas;
typedef std::vector<int> Stack;
typedef std::vector<Node*> Pile;
typedef std::vector<Node*> MAP;
typedef std::string transitionSequence;
typedef std::pair<transitionSequence, Node*> transSeqLang;

class Graph {

public:
    Node* initialNode;
    vector<Node*> states;
    inline static Stack stack;
    inline static Tas tas;
    map<string, int> transitions;
    map<Node*, vector<transSeqLang>> Lg;

    Graph();
    Graph(string file);
    Graph(Node* initialNode);
    virtual ~Graph();
    pair<vector<transSeqLang>, vector<int>> computeLanguage(Node* n);
    pair<vector<transSeqLang>, vector<int>> getLanguage(Node* n, bool firstCall);
    void generateRandomGraph(int NOV, int numEdges, int numTransitions);
    void printGraph();
    void printGraphFile();
    int nbPaths = 0;
    vector<int> findLocation(string sample, char findIt);

    // New methods for regular expression generation
    vector<transSeqLang> RegularExpressionGeneration(Pile& st, MAP& m);
    bool isInTheMap(MAP& m, Node *node);
    void UpdateLanguage(Node* s, MAP& m);
    void generatePrefixes();
    void generatePrefixes2();

};

#endif //IMPLEM_GRAPH_H
