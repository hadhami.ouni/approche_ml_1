
#include <iostream>
#include <vector>
#include <cstring>
#include <stack> 
#include <algorithm>
#include "Graph.h"
#include <cmath>
#include <fstream>
#include <queue>
#include "PathGenerator.h"

vector<int> findLocationspace(string sample, char findIt)
{
    vector<int> characterLocations;
    for (int i = 0; i < sample.size(); i++)
    {
        if (sample[i] == findIt)
        {
            characterLocations.push_back(i);
        }
    }
    return characterLocations;
}

/***********************************************************/
/*                      constucors                         */
/***********************************************************/
Graph::Graph(){};

Graph::Graph(Node *n) : initialNode(n)
{
}

Graph::~Graph() = default;

/////////////////////       constructor from a file    /////////////////////////////

Graph::Graph(string file)
{

    ifstream flux(file); // Ouverture du fichier
    if (flux)
    {

        string ligne;
        vector<int> location;
        map<string, int> map_nodes; // map for name and the num of nodes in g.states

        // Node * n = new Node(1381,'ag_1381');

        while (getline(flux, ligne)) // read line by line
        {

            location = findLocationspace(ligne, ' ');
            if (location.size() > 2) // no last sentence
            {
                string ag_source = ligne.substr(0, location[0]);
                int n_ag_source = std::stoi(ag_source.substr(3, ag_source.length() - 3));

                string ag_fin = ligne.substr(location[1] + 1, location[2] - location[1] - 1);
                int n_ag_fin = std::stoi(ag_fin.substr(3, ag_source.length() - 2));

                string transition = ligne.substr(location[6] + 2, ligne.length() - location[6] - 4) + "_";

                if (map_nodes.find(ag_source) == map_nodes.end())
                { // node doesn't exist
                    Node *n = new Node(n_ag_source, ag_source);
                    states.push_back(n);
                    map_nodes.insert(pair(ag_source, states.size() - 1));
                    // cout<<"add to map "<<ag_source<<"---"<<states.size()-1<<endl;
                }
                if (map_nodes.find(ag_fin) == map_nodes.end())
                { // node doesn't exist
                    Node *n = new Node(n_ag_fin, ag_fin);
                    states.push_back(n);
                    map_nodes.insert(pair(ag_fin, states.size() - 1));
                    // cout<<"add to map "<<ag_fin<<"---"<<states.size()-1<<endl;
                }

                trNode t;
                t.first = transition;

                // ajouter trnode  transitions(map)
                if (transitions.find(t.first) == transitions.end())
                { // add new transition with number tr

                    transitions.insert({t.first, transitions.size()});
                    // cout <<"ajouter une trnode " << t.first<<" avec un num "<<transitions.size()<< endl;
                }
                // cout<<"la position de source "<<map_nodes[ag_source]<<endl;
                // cout<<"la position de a fin "<<map_nodes[ag_fin]<<endl;
                t.second = states[map_nodes[ag_fin]];
                states[map_nodes[ag_source]]->getSuccessors().push_back(t);
            }

            // add the initial node
            std::size_t found = ligne.find("[initialstate]");
            if (found != std::string::npos)
            {
                cout << "c'est l'intialisation " << map_nodes[ligne.substr(0, found)] << endl;
                initialNode = states[map_nodes[ligne.substr(0, found)]];
            }
        }
    }

    else
    {
        cout << "ERREUR while opening the file !" << endl;
    }
}


string RandomString(int ch) {
    const char alpha[] = "abcdefghijklmnopqrstuvwxyz";
    std::string result;
    for (int i = 0; i < ch; ++i) {
        result += alpha[rand() % 26];
    }
    return result;
}

void Graph::generateRandomGraph(int NOV, int numEdges, int numTransitions)
{
    srand(time(0)); // Seed the random number generator

    // Create nodes
    for (int i = 0; i < NOV; i++)
    {
        Node *n = new Node(i);
        this->states.push_back(n);
    }
    
    // Generate a vector of transitions
    std::vector<std::string> transitionPool;
    for (int i = 0; i < numTransitions; i++) {
        transitionPool.push_back(RandomString(2) + "_");
    }

    // Track used transitions for each node to avoid duplicates
    std::map<Node*, std::set<std::string>> usedTransitions;

    // Ensure connectivity by creating a tree first
    std::set<int> connectedNodes;
    connectedNodes.insert(0); // Start with the first node
    std::vector<int> unconnectedNodes;
    for (int i = 1; i < NOV; ++i) {
        unconnectedNodes.push_back(i);
    }

    int nbEdges = 0;

    while (!unconnectedNodes.empty() && nbEdges < numEdges) {
        // Pick a random node from the connected set
        int sourceIndex = *std::next(connectedNodes.begin(), rand() % connectedNodes.size());
        Node* sourceNode = this->states[sourceIndex];

        // Pick a random node from the unconnected set
        int unconnectedIndex = rand() % unconnectedNodes.size();
        int targetIndex = unconnectedNodes[unconnectedIndex];
        Node* targetNode = this->states[targetIndex];

        // Remove the target node from the unconnected set and add to the connected set
        unconnectedNodes.erase(unconnectedNodes.begin() + unconnectedIndex);
        connectedNodes.insert(targetIndex);

        // Ensure the chosen transition is unique for the current node
        std::string transition;
        do {
            transition = transitionPool[rand() % transitionPool.size()];
        } while (usedTransitions[sourceNode].count(transition) > 0);

        // Mark the transition as used for the current node
        usedTransitions[sourceNode].insert(transition);

        // Add the transition
        sourceNode->getSuccessors().emplace_back(transition, targetNode);
        this->transitions.insert({transition, this->transitions.size()});
        nbEdges++;
    }

    // Add additional edges randomly
    while (nbEdges < numEdges) {
        // Choose a random source node
        int sourceIndex = rand() % NOV;
        Node* sourceNode = this->states[sourceIndex];

        // Ensure the chosen transition is unique for the current node
        std::string transition;
        do {
            transition = transitionPool[rand() % transitionPool.size()];
        } while (usedTransitions[sourceNode].count(transition) > 0);

        // Mark the transition as used for the current node
        usedTransitions[sourceNode].insert(transition);

        // Choose a random target node
        Node* targetNode = this->states[rand() % NOV];

        // Add the transition
        sourceNode->getSuccessors().emplace_back(transition, targetNode);
        this->transitions.insert({transition, this->transitions.size()});
        nbEdges++;
    }

    this->initialNode = this->states[0];

    // Print graph statistics
    std::cout << "Informations du graphe : " << std::endl;
    std::cout << "Nodes : " << this->states.size() << " | Edges créés : " << nbEdges << " | Labels utilisés : " << this->transitions.size() << std::endl;
}

void Graph::printGraph()
{
    cout << "\nThe generated graph is: ";
    for (int i = 0; i < this->states.size(); i++)
    {
        cout << "\n\t" << i << "-> { ";
        for (auto v : this->states[i]->getSuccessors())
        {
            cout << v.first << "," << v.second->getId() << "	";
        }
        cout << " }";
    }
    cout << endl;
}

void Graph::printGraphFile()
{
    for (int i = 0; i < this->states.size(); i++)
    {
        cout << "\n\t" << states[i]->getName() << "," << states[i]->getId() << " -> { ";
        for (auto v : this->states[i]->getSuccessors())
        {
            cout << v.first << "," << v.second->getName() << "," << v.second->getId() << "	";
        }
        cout << " }";
    }
    cout << endl;
}

int compt=0;
vector<transSeqLang> Graph::RegularExpressionGeneration(Pile &st, MAP& m)
{
   // cout<<"Appel récurif num : "<<compt++<<endl;
   // cout<<"taille de pile : "<<st.size()<<" , taille de map : "<<m.size()<<endl;
    if (!st.empty())
    {
        
        Node *cur = st.back();

        //cout<<" (2) cur is null ? "<< (cur==nullptr) <<endl;

        cout<<"Noeud : "<<cur->getName()<<endl;

        // Check if cur has no successors
        if (cur->getSuccessors().empty())
        {
            cur->CompCy.push_back({"", nullptr}); // <∅,∅>
        }

        // Process each successor of cur
        for (auto &next : cur->getSuccessors()) //foreach next ∈ succ(cur) 
        {
            Node *nextNode = next.second;

            if (find(st.begin(), st.end(), nextNode) != st.end()) //if next ∈ st 
            {
                //cout<< "label cur, next : "<<next.first<<" |  next node : "<< nextNode->getName()<<endl;
                if(nextNode!=cur)
                    cur->CompCy.push_back({next.first, nextNode});
                else
                    cur->Cy.push_back({next.first, nextNode});
            }
            else
            {
                if (isInTheMap(m,nextNode)) //nextNode n'existe ni dans Cy ni dans ComCy
                {
                    cout<<" -- Appel update language "<<endl;
                    UpdateLanguage(nextNode,m);
                    cout<<" -- Fin Appel update language "<<endl;

                }
                else
                {
                    st.push_back(nextNode);
                    RegularExpressionGeneration(st,m);
                }

                //cout<<"----" << cur->getName() <<"---->"<<nextNode->getName() <<endl;

                for (auto &lg : nextNode->Lg)
                {
                
                    if(lg.second!=cur)
                        cur->CompCy.push_back({next.first + lg.first, lg.second});
                    else
                        cur->Cy.push_back({next.first + lg.first, lg.second});

                    //concatèner l'étiquette de cur vers nextNode avec l'étiquette de nextNode vers son successeur (lg.first).
                    //lg.second est le nœud successeur dans nextNode->CompCy, qui devient le successeur dans cur->CompCy.
                }
            }
        }
/*
        cout<<"Fin traitement successeur de l'etat : "<<cur->getName()<<endl;
        cout<<"taille de Lg : "<<cur->Lg.size()<<endl;

        cout<<" **** taille de Lg : "<<cur->Lg.size() <<"pour le node :" <<cur->getName()<<endl;

        cout<<"taille de Cy : "<<cur->Cy.size()<<endl;
        cout<<"taille de ComCy : "<<cur->CompCy.size()<<endl;

*/
        // Check if Cy(cur) ≠ ∅
        if (!cur->Cy.empty())
        {
            // Check if CompCy(cur) ≠ ∅
            if (!cur->CompCy.empty())
            {
                // Lg(cur) = union for each e ∈ CompCy(cur) { < (Πe′ ∈ Cy(cur)(e′.seq)* )* . e.seq, e.state > }
                //vector<transSeqLang> newLg;
                string unionExpr="";

                if(cur->Cy.size()>1){
                    unionExpr="(";
                    for (auto &e_prime : cur->Cy)
                    {
                        unionExpr += "(" + e_prime.first + ")*";
                    }
                    unionExpr+=")*";
                }
                else{
                    for (auto &e_prime : cur->Cy)
                    {
                        unionExpr += "(" + e_prime.first + ")*";
                    }
                }
                    
                for (auto &e : cur->CompCy)
                {
                    /*string unionExpr="(";
                    for (auto &e_prime : cur->Cy)
                    {
                        unionExpr += "(" + e_prime.first + ")*";
                    }
                    unionExpr += e.first;*/
                    cur->Lg.push_back({unionExpr+e.first, e.second});
                }
    
                //cur->Lg = newLg;
            }
            else
            {
                string unionExpr="";

                // Lg(cur) = { < (Πe ∈ Cy(cur)(e.seq)* )* , ∅ > }
                if(cur->Cy.size()>1){
                    unionExpr="(";
                    for (auto &e : cur->Cy)
                    {
                        unionExpr += "(" + e.first + ")*";
                    }
                    unionExpr += ")*";
                }
                else{
                    for (auto &e : cur->Cy)
                    {
                        unionExpr += "(" + e.first + ")*";
                    }
                }
                
                cur->Lg.push_back({unionExpr, nullptr});
            }
        }
        else {

            cur->Lg = cur->CompCy ;
        }

    cur->printCyandComp();
    st.pop_back();
    m.push_back(cur);

    
    //cur->printLg();
    return cur->Lg;
    }
}

bool Graph::isInTheMap(MAP& m, Node *node){
    if (find(m.begin(), m.end(), node) != m.end())
    return true;
    return false;
}

void Graph::UpdateLanguage(Node* s, MAP& m) {
    vector <transSeqLang> newLg;

    //cout<<"Update Lg appelé pour le node : "<<s->getName() <<endl<< "Lg avant update : " << endl;
    //s->printLg();

    for (auto &e : s->Lg) {
        if (e.second != nullptr && e.second != initialNode && isInTheMap(m,e.second) ) {
            
            //cout<<"appel récurif update Lg pour "<< e.second->getName()<<endl;

            UpdateLanguage(e.second,m);
            
            // Lg(s).replace(e, e.seq.Lg(e.state))
            for (auto &l : e.second->Lg){

                //cout<<"-->"<<e.second->getName()<<endl;
                newLg.push_back({e.first+l.first, l.second});
                
                //cout<<"transition : "<<e.first+l.first<< "Noeud : "<< l.second->getName()<<endl;
                //ici j’ai remplacé les accolades par des parenthèse, je ne suis pas sur car il faut insérer une paire … à vérifier
            }
        }
	else
	    newLg.push_back(e);
    }
    s->Lg.clear();
    s->Lg=newLg;

    //cout<<"Lg après update : "<<endl;
    //s->printLg();

}

void updatePrefix(Node * currentNode, string label){
    string prefixe, transitionLabel;



        //std::cout << "-- UPDATE for Current node : " << currentNode->getId() <<" label : "<<label<< std::endl;
            //cout << "Prefixes ending with label \"" << label << "\" : ";
            for (auto& n : currentNode->prefixes) {
                if (n.size() >= label.size() && n.compare(n.size() - label.size(), label.size(), label) == 0) {
                    //cout << n << " | ";
                }
            }
           // cout << endl;

            for (auto& successor : currentNode->getSuccessors()) {
            Node* nextNode = successor.second;
            transitionLabel = successor.first;

            //cout << "AVANT UPDT : prefixes du noeud " << nextNode->getId() << " : ";
            for (auto& n : nextNode->prefixes) {
                //cout << n << " | ";
            }
            //cout<<endl;

            for (auto& n : currentNode->prefixes) {
                if (n.size() >= label.size() && n.compare(n.size() - label.size(), label.size(), label) == 0) {
                    prefixe = n + transitionLabel;
                    nextNode->prefixes.push_back(prefixe);
                }  
            }

            //cout << "AVANT UPDT : prefixes du noeud " << nextNode->getId() << " : ";
            for (auto& n : nextNode->prefixes) {
                //cout << n << " | ";
            }
            //cout<<endl;

            }

/*
            
            for (auto& n : currentNode->prefixes) {
                if (n!=""){
                    prefixe =  n + label ;
                    currentNode->prefixes.push_back(prefixe);
                }
                
            }
            */
            //cout << "APRES UPDT : prefixes du noeud " << currentNode->getId() << " : ";
            for (auto& n : currentNode->prefixes) {
                //cout << n << " | ";
            }
            //cout << endl << "-------------------" << endl;
        
}

void Graph::generatePrefixes() {
    std::queue<Node*> nodeQueue;  // Utilisation de std::queue pour BFS
    std::queue<Node*> cycleQueue;  // Utilisation de std::queue pour BFS
    std::set<Node*> visitedNodes; // Utilisation de std::set pour suivre les nœuds visités
    std::set<Node*> nodesInQueue; // Utilisation de std::set pour suivre les nœuds dans la file d'attente
    Node* initialNode = this->initialNode;

    initialNode->prefixes.push_back(""); 

    nodeQueue.push(initialNode);
    visitedNodes.insert(initialNode);
    nodesInQueue.insert(initialNode);

    string prefixe;
    set<string> prefixes;
    string transitionLabel = "";

    while (!nodeQueue.empty()) {
        Node* currentNode = nodeQueue.front();
        nodeQueue.pop();
        nodesInQueue.erase(currentNode);

        //cout << endl << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;
        //std::cout << "-- Current node : " << currentNode->getId() << std::endl;

        for (auto& successor : currentNode->getSuccessors()) {
            Node* nextNode = successor.second;
            transitionLabel = successor.first;

            // Afficher le label de la transition
            //std::cout << "Transition: " << transitionLabel << " -> Node ID: " << nextNode->getId() << std::endl;

            for (auto& n : currentNode->prefixes) {
                prefixe = n + transitionLabel;
                nextNode->prefixes.push_back(prefixe);
            }

            if (visitedNodes.find(nextNode) == visitedNodes.end() && nodesInQueue.find(nextNode) == nodesInQueue.end()) { //si nextNode n'a pas encore été visité et n'est pas dans la file.
                // Marquer le nœud comme visité et ajouter à la file
                visitedNodes.insert(nextNode);
                nodesInQueue.insert(nextNode);
                //cout << "oui " << endl;
                nodeQueue.push(nextNode);
            } else if (visitedNodes.find(nextNode) != visitedNodes.end() && nodesInQueue.find(nextNode) == nodesInQueue.end()) {
                // Nœud déjà visité, pas dans la file, mais toujours avec des successeurs
                //cout << "######################## visited with successors: Node ID " << nextNode->getId() << " label : "<<transitionLabel<<endl;
                cycleQueue.push(nextNode);
                updatePrefix(nextNode, transitionLabel);
            }

            //cout << "prefixes du noeud " << nextNode->getId() << " : ";
            for (auto& n : nextNode->prefixes) {
                //cout << n << " | ";
            }
           // cout << endl << "-------------------" << endl;
        }
    }
    
}

//a supprimer, resultats pas comme expected
void Graph::generatePrefixes2() {
    std::queue<Node*> nodeQueue;  // Utilisation de std::queue pour BFS
    std::set<Node*> visitedNodes; // Utilisation de std::set pour suivre les nœuds visités
    Node* initialNode = this->initialNode;
    nodeQueue.push(initialNode);
    visitedNodes.insert(initialNode);

    while (!nodeQueue.empty()) {
        Node* currentNode = nodeQueue.front();
        nodeQueue.pop();

        // Process current node
        //std::cout << "Node : " << currentNode->getId() << ", son Lg : "<< std::endl;
       
        //currentNode->printLg();
        vector<transSeqLang> languages=currentNode->Lg;
       // cout<<"Prefixes : "<<endl;
        while (!languages.empty())
        {

            for (string &path : PathGenerator::extractPaths(languages[languages.size() - 1].first))
            {
                //cout<<"p : "<<path<<endl;
            }
            languages.pop_back();
        }
        // Process each successor of the current node
        for (auto &successor : currentNode->getSuccessors()) {
            Node* nextNode = successor.second;

            // Only process the successor if it has not been visited yet
            if (visitedNodes.find(nextNode) == visitedNodes.end()) {
                // Mark the successor as visited and add it to the queue
                visitedNodes.insert(nextNode);
                nodeQueue.push(nextNode);
            }
        }
    }
}