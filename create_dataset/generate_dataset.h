#ifndef GENERATE_DATASET_H
#define GENERATE_DATASET_H

#include <set>
#include <vector>
#include <string>

// Fonction pour diviser une chaîne en vecteur de chaînes en utilisant un délimiteur
std::vector<std::string> split(const std::string &s, char delimiter);

// Fonction pour calculer les prochains choix possibles en fonction du préfixe et des séquences d'observations
std::set<std::string> best_choice(const std::string &prefix, const std::set<std::string> &choices, const std::vector<std::string> &set_of_opt);

// Fonction pour générer des préfixes aléatoires à partir des chemins générés
std::vector<std::string> generate_prefixes(const std::vector<std::string>& generatedPaths);

#endif // GENERATE_DATASET_H
