#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <sstream>

using namespace std;

// Structure pour contenir les données
struct DataEntry {
    string prefix;
    set<string> choices;
    string result;
};

// Fonction pour convertir un set de strings en une seule chaîne avec un délimiteur
string setToString(const set<string>& s, char delimiter) {
    stringstream ss;
    for (auto it = s.begin(); it != s.end(); ++it) {
        if (it != s.begin()) {
            ss << delimiter;
        }
        ss << *it;
    }
    return ss.str();
}

// Fonction pour écrire le vecteur de DataEntry dans un fichier CSV
void writeCSV(const vector<DataEntry>& data, const string& filename) {
    ofstream file(filename);
    if (!file.is_open()) {
        cerr << "Failed to open file: " << filename << endl;
        return;
    }

    // Écrire l'en-tête du fichier CSV
    file << "prefix;choices;result\n";

    // Écrire les données
    for (const auto& entry : data) {
        file << entry.prefix << ";"
             << setToString(entry.choices, ',') << ";"
             << entry.result << "\n";
    }

    file.close();
    cout << "CSV file written: " << filename << endl;
}

int main() {
    // Exemple de données
    vector<DataEntry> data = {
        {"4_2_4_", {"1", "2", "3"}, "2"},
        {"2_0_1_", {"4", "5", "6"}, "5"},
        {"2_0_1_", {"4", "5", "6"}, "4"},
        {"7_8_2_", {"7", "8", "9"}, ""}
    };

    // Écrire les données dans un fichier CSV
    writeCSV(data, "output.csv");

    return 0;
}
