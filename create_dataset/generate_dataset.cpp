#include "generate_dataset.h"
#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib> // Pour rand()
#include <ctime>   // Pour time()


using namespace std;


// Fonction pour calculer les prochains choix possibles en fonction du préfixe et des séquences d'observations
set<string> best_choice(const string &prefix, const set<string> &choices, const vector<string> &set_of_opt)
{
    set<string> possibleChoices; // Utilisé pour stocker les prochains choix possibles

    vector<string> prefix_vec = split(prefix, '_');

    // Parcourir chaque séquence dans set_of_opt
    for (const auto &obs : set_of_opt)
    {
        vector<string> obs_vec = split(obs, '_');

        // vérifier si les premiers éléments de l'ensemble obs correspondent exactement aux éléments de l'ensemble prefix.
        if (equal(prefix_vec.begin(), prefix_vec.end(), obs_vec.begin()))
        {
            // Si l'observation est au moins de la taille du préfixe + 1
            if (obs_vec.size() > prefix_vec.size())
            {
                string next = obs_vec[prefix_vec.size()]; // Utilisez l'index correct pour obtenir l'élément suivant après le préfixe

                // Enlever l'underscore final du choix s'il existe
                if (!next.empty() && next.back() == '_')
                {
                    next.pop_back();
                }

                // Vérifiez si le choix est dans l'ensemble des choix possibles
                if (choices.find(next) != choices.end())
                {
                    possibleChoices.insert(next); // Ajouter le choix possible
                }
            }
        }
    }

    return possibleChoices;
}
// Fonction pour diviser une chaîne en vecteur de chaînes en utilisant un délimiteur
vector<string> split(const string &s, char delimiter)
{
    vector<string> tokens;
    size_t start = 0, end = s.find(delimiter);
    while (end != string::npos)
    {
        tokens.push_back(s.substr(start, end - start + 1)); // Inclure le délimiteur
        start = end + 1;
        end = s.find(delimiter, start);
    }
    if (start < s.size()) // Ajouter le dernier token s'il existe
    {
        tokens.push_back(s.substr(start) + delimiter);
    }
    return tokens;
}

// Fonction pour générer des préfixes aléatoires à partir des chemins générés
vector<string> generate_prefixes(const vector<string>& generatedPaths) {
    srand(time(NULL)); // Initialisation de la graine pour rand()

    set<string> unique_prefixes;

    // Générer des préfixes aléatoires à partir des chemins
    for (const auto& path : generatedPaths) {
        if (path.empty()) continue;

        // Diviser le chemin en transitions
        vector<string> transitions = split(path, '_');

     

        // Nombre aléatoire de préfixes à générer pour chaque chemin (entre 1 et la taille des transitions)
        int num_prefixes = rand() % transitions.size() + 1;

        // Générer chaque préfixe aléatoire
        for (int i = 0; i < num_prefixes; ++i) {
            // Longueur aléatoire du préfixe (entre 1 et la taille des transitions)
            int prefix_length = rand() % transitions.size() + 1;

            // Point de départ aléatoire pour le préfixe dans les transitions
            int start_index = rand() % (transitions.size() - prefix_length + 1);

            // Construire le préfixe à partir des transitions
            string prefix;
            for (int j = 0; j < prefix_length; ++j) {
                prefix += transitions[start_index + j];
            }
            unique_prefixes.insert(prefix);
        }
    }

    // Convertir l'ensemble des préfixes uniques en vecteur
    vector<string> prefixes(unique_prefixes.begin(), unique_prefixes.end());

    return prefixes;
}

/*
int main()
{
    string prefix = "1_5_9_4_8_";
    set<string> choices = {"1", "2", "3", "0"};
    vector<string> set_of_opt = {
        "1_5_9_4_8_0_8_1_",
        "1_5_9_4_2_2_",
        "1_5_9_4_7_"};

    set<string> best = best_choice(prefix, choices, set_of_opt);
    cout << "Next possible choices: ";
    for (const auto &choice : best)
    {
        cout << choice << " ";
    }
    cout << endl;

    return 0;
}
*/


