#include <iostream>
#include "Node.h"
#include "Graph.h"
#include "PathGenerator.h"
#include "or-tools/ortools/linear_solver/linear_solver.h"
#include <fstream>
#include <string>
#include <cstdlib>
#include <map>
#include <queue>

// Fonction pour vérifier si une chaîne est numérique
bool isNumeric(std::string const &str)
{
    auto it = str.begin();
    while (it != str.end() && std::isdigit(*it))
    {
        it++;
    }
    return !str.empty() && it == str.end();
}

// Fonction pour obtenir le temps actuel en secondes
double getTime() { return (double)clock() / (double)CLOCKS_PER_SEC; }

// Fonction de split pour diviser une chaîne en utilisant un délimiteur
vector<string> split(const string &str, char delimiter) {
   vector<string> tokens;
   string token;
   istringstream tokenStream(str);
   while (getline(tokenStream, token, delimiter)) {
       tokens.push_back(token);
   }
   return tokens;
}


// Fonction pour calculer les prochains choix possibles en fonction du préfixe et des séquences d'observations
set<string> best_choice(const string &prefix, const set<string> &choices, const vector<string> &set_of_opt) {
   set<string> possibleChoices; // Utilisé pour stocker les prochains choix possibles
   vector<string> prefix_vec = split(prefix, '_');

   // Parcourir chaque séquence dans set_of_opt
   for (const auto &obs : set_of_opt) {
       vector<string> obs_vec = split(obs, '_');


       // vérifier si les premiers éléments de l'ensemble obs correspondent exactement aux éléments de l'ensemble prefix.
       if (equal(prefix_vec.begin(), prefix_vec.end(), obs_vec.begin())) {
           // Si l'observation est au moins de la taille du préfixe + 1
           if (obs_vec.size() > prefix_vec.size()) {
               string next = obs_vec[prefix_vec.size()]; // Utilisez l'index correct pour obtenir l'élément suivant après le préfixe
               
               // Remettre l'underscore à la fin pour correspondre avec les choix possibles
               next += '_';
               
               // Vérifiez si le choix est dans l'ensemble des choix possibles
               if (choices.find(next) != choices.end()) {
                   possibleChoices.insert(next); // Ajouter le choix possible
               }
           }
       }
   }

   return possibleChoices;
}

// Fonction pour convertir un set de strings en une seule chaîne avec un délimiteur
string setToString(const set<string>& s, char delimiter) {
    stringstream ss;
    for (auto it = s.begin(); it != s.end(); ++it) {
        if (it != s.begin()) {
            ss << delimiter;
        }
        ss << *it;
    }
    return ss.str();
}
void generate_dataset(Graph *g, vector<string> optimalPaths, string filename) {
    g->generatePrefixes();

    std::queue<Node*> nodeQueue;  // Utilisation de std::queue pour BFS
    std::set<Node*> visitedNodes; // Utilisation de std::set pour suivre les nœuds visités
    Node* initialNode = g->initialNode;
    nodeQueue.push(initialNode);
    visitedNodes.insert(initialNode);
    cout << "debut generate dataset !! " << endl;

    set<string> choices;
    set<string> choix;

    ofstream file(filename);
    if (!file.is_open()) {
        cerr << "Failed to open file: " << filename << endl;
        return;
    }
    // Écrire l'en-tête du fichier CSV
    file << "prefix;choices;result\n";

    while (!nodeQueue.empty()) {
        Node* currentNode = nodeQueue.front();
        nodeQueue.pop();

        // Process current node
        //std::cout << "xxxx node : " << currentNode->getId() << std::endl;
        choices.clear();
        for (auto &n : currentNode->getSuccessors()) {
            std::string trans = n.first;
            choices.insert(trans);
        }

        for (auto &prefixe : currentNode->prefixes) {
            //cout << "P : " << prefixe << endl;
            choix = best_choice(prefixe, choices, optimalPaths);

            file << prefixe << ";"
                 << setToString(choices, '|') << ";"
                 << setToString(choix, '|') << "\n";
        }

        // Process each successor of the current node
        for (auto &successor : currentNode->getSuccessors()) {
            Node* nextNode = successor.second;

            // Only process the successor if it has not been visited yet
            if (visitedNodes.find(nextNode) == visitedNodes.end()) {
                // Mark the successor as visited and add it to the queue
                visitedNodes.insert(nextNode);
                nodeQueue.push(nextNode);
            }
        }
    }

    file.close();
    cout << "CSV file written: " << filename << endl;
}

void printHelp()
{
    std::cout << "/**************************************************************/\n"
              << "/********************         help         ********************/\n"
              << " -To generate a random graph: use the options --nb-nodes <n> --nb-edges <m> --nb-labels <l>\n"
              << " -To extract the optimal solution of a .dot file: use the option --input-file <filename.dot>\n"
                << " -To extract the optimal solution of a .net file after generating its reachability graph: use the option -r <filename.net>\n"
                << " -To extract the optimal solution of a .net file after generating its SOG: use the option -c <filename.net>\n"

              << "/**************************************************************/\n";
}

int main(int argc, char *argv[])
{
    Pile p;
    MAP m;

    // Afficher l'aide si aucun argument n'est fourni
    if (argc < 2 || std::string(argv[1]).find("help") != -1)
    {
        printHelp();
        return 0;
    }

    bool fromfile = false;
    Graph *g = new Graph();
    bool randomgen = false;
    std::string nom = "default";

    // Dictionnaire pour stocker les arguments
    std::map<std::string, int> argsMap;
    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if (arg == "--nb-nodes" || arg == "--nb-edges" || arg == "--nb-labels")
        {
            if (i + 1 < argc && isNumeric(argv[i + 1]))
            {
                argsMap[arg] = std::stoi(argv[i + 1]);
                i++;
            }
            else
            {
                std::cerr << "Invalid value for " << arg << std::endl;
                return 1;
            }
        }
    }

    // Vérifier si tous les arguments nécessaires sont fournis pour générer un graphe aléatoire
    if (argsMap.size() == 3)
    {
        randomgen = true;
        g->generateRandomGraph(argsMap["--nb-nodes"], argsMap["--nb-edges"], argsMap["--nb-labels"]);
    }
    else
    {
        std::string option = std::string(argv[1]);

        if (option == "--input-file")
        {
            std::string filePath = argv[2];
            int posDot = filePath.find(".dot");
            int posSlash = filePath.rfind('/'); // trouve la position du dernier '/'
            nom = filePath.substr(posSlash + 1, posDot - posSlash - 1);

            // Charger le graphe depuis un fichier .dot
            g = new Graph(filePath);
        }
        else if (option == "-r" || option == "-c")
        {
            // Traiter les options -r et -c
            std::string filePath = argv[2];
            int posDot = filePath.find(".n");
            int posSlash = filePath.rfind('/');
            nom = filePath.substr(posSlash + 1, posDot - posSlash - 1);

            // Déterminer l'option à passer à StructAnalysis
            std::string optionSA;
            if (option == "-r") optionSA = "-r";
            else if (option == "-c") optionSA = "-c";

            // Exécuter l'analyse structurelle avec l'option appropriée
            std::string cmd = "./StructAnalysis " + optionSA + " " + filePath;
            int result = std::system(cmd.c_str());

            if (result != 0) {
                std::cerr << "Error executing StructAnalysis command!" << std::endl;
                return 1;
            }

            // Générer le nom du fichier .dot après l'analyse, selon l'option choisie
            std::string nom_dot;
            if (option == "-r") {
                nom_dot = "./result/reach_" + nom + ".dot";
            } else if (option == "-c") {
                nom_dot = "./result/complete_sog_" + nom + ".dot";
            }
            
            std::cout << "The dot file is stored in: " << nom_dot << std::endl;

            // Vérifier si le fichier .dot existe
            std::ifstream infile(nom_dot);
            if (!infile.good()) {
                std::cerr << "ERROR while opening the file: " << nom_dot << std::endl;
                return 1;
            }

            // Charger le graphe depuis le fichier .dot généré
            g = new Graph(nom_dot);
        }
        else
        {
            std::cerr << "Invalid option provided." << std::endl;
            printHelp();
            return 1;
        }

        fromfile = true;
    }

/*
    // print the Graph
    if (!fromfile)
        g->printGraph();
    else
        g->printGraphFile();

*/
    // vector of generated paths
    vector<string> generatedPaths, generatedPathsnnull, generatedPrefixes, optimalPaths;
    int nb=0;



    // Initialize the stack with the initial state
    p.push_back(g->initialNode);

    float start_time = getTime();

    vector<transSeqLang> languages=g->RegularExpressionGeneration(p,m);

    cout<<"FIN EXPREG"<<endl;

    //cout<<"nb d'elems de LG "<< g->initialNode->sizeLg()<<endl;
    //cout<<"Longueur : "<<g->initialNode->getLanguage().size()<<endl;
    //g->initialNode->printLg();
    //cout<< "initial node is : "<<g->initialNode->getName()<<endl;
    
    cout<<"Regular Expression of the system language : "<<endl<<g->initialNode->getLanguage()<<endl;

   // vector<transSeqLang> languages = g->computeLanguage(g->initialNode).first;

    while (!languages.empty())
    {

        for (string &path : PathGenerator::extractPaths(languages[languages.size() - 1].first))
        {
            generatedPaths.push_back(path);
        }
        languages.pop_back();
    }
    
    
    /*

    cout << "Print all generated paths" << endl;
    for (int i = 0; i < generatedPaths.size(); i++)
        std::cout << i << ": " << generatedPaths[i] << endl;
    */

 
    int nonulp = 0; // not null paths
    for (int i = 0; i < generatedPaths.size(); i++)
        if (generatedPaths[i].size() != 0)
            generatedPathsnnull.push_back(generatedPaths[i]);

    cout << "nb of no null paths " << generatedPathsnnull.size() << endl;
    
      cout << "Print all generated no null paths" << endl;
    for (int i = 0; i < generatedPathsnnull.size(); i++)
        std::cout << i << ": " << generatedPathsnnull[i] << endl;



    /*cout << "Print all generated prefixes" << endl;
    for (int i = 0; i < generatedPrefixes.size(); i++)
        std::cout << i << ": " << generatedPrefixes[i] << endl;*/

      int averageSize=0;
    for (int i = 0; i < generatedPathsnnull.size(); i++){
        int pathSize = std::count(generatedPathsnnull[i].begin(), generatedPathsnnull[i].end(), '_');
        //std::cout << i << ": " << generatedPathsnnull[i] << " de longueur  : " << pathSize <<endl;
        averageSize+=pathSize;
    }
    averageSize=averageSize/generatedPathsnnull.size();
    cout<<"Average Path size : "<<averageSize<<endl;

    int lignes = g->transitions.size();
    int colonnes = generatedPathsnnull.size();
    cout << "Taille de la Matrice des contraintes : "<<endl<<"(" << lignes<< " , " << colonnes <<")" << endl;

    // Allocation dynamique de la matrice
    int **constraints = new int*[lignes];
    for(int i = 0; i < lignes; i++) {
        constraints[i] = new int[colonnes];
    }

    cout<<"init de la matrice des contraintes ..."<<endl;

    // nitialisation
    for(int i = 0; i < lignes; i++) {
        for(int j = 0; j < colonnes; j++) {
            constraints[i][j] = 0;
        }
    }
    
    
    //charge_constraint_matrix(generatedPathsnnull, g->transitions, constraints);

    string transition;

    // Charge constraints Matrix with paths from regex and generate minPaths

    for (int i = 0; i < generatedPathsnnull.size(); i++)
    {
        int pos = -1; // position de _ pour copier transition
        for (int j = 0; j < generatedPathsnnull[i].length(); j++)
        {
            if (generatedPathsnnull[i][j] == '_')
            {
                transition.clear();
             
                transition = generatedPathsnnull[i].substr(pos + 1, j - pos);
                constraints[g->transitions[transition]][i] = 1;
                
                pos = j;
            }
        }
    }

    /*
    for (int i = 0; i < generatedPathsnnull.size(); i++)
        std::cout << i << ": " << generatedPathsnnull[i] << endl;
    */
    // Créez le solveur MIP avec le backend SCIP.
    std::unique_ptr<operations_research::MPSolver> solver(
        operations_research::MPSolver::CreateSolver("SCIP"));
    if (!solver)
    {
        std::cerr << "SCIP solver unavailable." << std::endl;
        return -1;
    }
    // Créez les variables.
    std::vector<const operations_research::MPVariable *> x(generatedPathsnnull.size() + g->transitions.size());
    for (int j = 0; j < generatedPathsnnull.size(); ++j)
    {
        x[j] = solver->MakeIntVar(0.0, 1, "x" + std::to_string(j));
    }
    cout<<"fin creation des variables .. "<<endl;


    // Créez les contraintes.
    int i = 0;
    for (const auto &transition_pair : g->transitions)
    {
        operations_research::MPConstraint *constraint = solver->MakeRowConstraint(1, solver->infinity(), "");
        std::cout << "Contrainte pour la transition: " << transition_pair.first << " : ";
        int transition_count = 0;
        for (int j = 0; j < generatedPathsnnull.size(); ++j)
        {
            if (constraints[transition_pair.second][j] == 1)
            {
                constraint->SetCoefficient(x[j], 1);
                std::cout << "x" << j << " + ";
                transition_count++;
            }
        }
        std::cout << " >= 1" << std::endl;
        i++;
    }
    cout<<"fin création des contraintes .."<<endl;
    // Create the objective function.
    operations_research::MPObjective *const objective = solver->MutableObjective();
    std::cout << "Objective : " << std::endl;
    for (int j = 0; j < generatedPathsnnull.size(); ++j)
    {
        // Set the coefficient for the objective function to the length of the path
        int path_size = std::count(generatedPathsnnull[j].begin(), generatedPathsnnull[j].end(), '_');
        objective->SetCoefficient(x[j], path_size);
        std::cout << path_size << " * x" << j;
        if (j < generatedPathsnnull.size() - 1)
        {
            std::cout << " + ";
        }
    }
    std::cout << std::endl;
    cout<<"fin fct objective .. "<<endl;
    // Set the objective function to minimization
    objective->SetMinimization();

    // Résoudre le problème.
    const operations_research::MPSolver::ResultStatus result_status = solver->Solve();

    // Vérifiez que le problème a une solution optimale.
    if (result_status != operations_research::MPSolver::OPTIMAL)
    {
        std::cout << "The problem does not have an optimal solution." << std::endl;
        return -1;
    }
    std::cout << "Solution:" << std::endl;
    std::cout << "Optimal objective value = " << objective->Value() << std::endl;
    
    // Libération de la mémoire allouée
    for(int i = 0; i < lignes; i++) {
        delete[] constraints[i]; 
    }
    delete[] constraints; 

    nb = 0;
    int avg_opt_size=0;
    if (!randomgen)
    {

        ofstream monFlux("./result/result_opt_" + nom + ".txt");

        for (int j = 0; j < generatedPathsnnull.size(); ++j)
        {

            cout << "x[" << j << "]=" << x[j]->solution_value()<< " ";
            if (x[j]->solution_value() == 1)
            {
                nb++;
                avg_opt_size+=generatedPathsnnull[j].size();
                optimalPaths.push_back(generatedPathsnnull[j]);
                monFlux << "Chemin optimal ( "<<nb<<" ): "<< generatedPathsnnull[j]<<" de longueur : "<<std::count(generatedPathsnnull[j].begin(), generatedPathsnnull[j].end(), '_')<<endl<<"----------------------------------"<<endl;
                cout << "Chemin optimal ( "<<nb<<" ): "<< generatedPathsnnull[j]<<" de longueur : "<<std::count(generatedPathsnnull[j].begin(), generatedPathsnnull[j].end(), '_')<<endl;

                monFlux<<"Regular Expression of the system language : "<<endl<<g->initialNode->getLanguage()<<endl;
                //cout<<"Regular Expression of the system language : "<<endl<<g->initialNode->getLanguage()<<endl;

            }
        }
        monFlux << "le nombre de chemins observés: " << nb << endl;
        cout << "le nombre de chemins observés : " << nb << endl;
        cout <<"Average optimal paths size : "<< avg_opt_size<<endl;
    }
    else{
        for (int j = 0; j < generatedPathsnnull.size(); ++j)
        {
            if (x[j]->solution_value() == 1)
            {
                avg_opt_size+=generatedPathsnnull[j].size();
                optimalPaths.push_back(generatedPathsnnull[j]);
                nb++;
                cout << "Chemin optimal ( "<<nb<<" ): "<< generatedPathsnnull[j]<<" de longueur : "<<std::count(generatedPathsnnull[j].begin(), generatedPathsnnull[j].end(), '_')<<endl;
            }
        }
        cout << "==> le nombre de chemins optimaux : " << nb << " parmi "<< generatedPathsnnull.size() <<" chemins " << endl;
        cout <<"Average optimal paths size : "<< avg_opt_size/nb<<endl;

    }

    cout << endl;


    //cout << "Statistics : ";
    //cout << solver->iterations()<<endl;
    
    float elapsed_time = getTime() - start_time;
    cout << "\nTotal time: " << elapsed_time << " seconds" << endl;

    string csvfile = "./dataset/"+nom+".csv";
    generate_dataset(g,optimalPaths,csvfile);
    //g->generatePrefixes();
    return 0;
    
}
